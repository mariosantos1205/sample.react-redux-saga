import Footer from './components/shared/Footer';
import Header from './components/shared/Header';
import TaskManager from './components/TaskManager'

function App({ store }) {
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col">
            <Header />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <TaskManager />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Footer />
          </div>
        </div>
      </div>
    </>
  )
}

export default App;
