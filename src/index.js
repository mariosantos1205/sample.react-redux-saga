import React from 'react';
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import App from './App';
import store from './store'

const render = () => {
  ReactDOM.render(    
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  );
}

render()
store.subscribe(render)