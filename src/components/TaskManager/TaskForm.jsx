import React from 'react';
import { connect } from 'react-redux'

import {
  Form, FormGroup, Input, Label, Button
} from 'reactstrap'
import { addTaskAction } from '../../actions/taskActions'
import Task from '../../models/Task';

const newTask = new Task(7, 'teste', 'yyyyyy')

const TaskForm = (props) => {
  
  const handlerAddTask = (ev) => {
    ev.preventDefault()
    props.addTask(newTask)
  }
  
  return (
    <Form onSubmit={handlerAddTask}>
      <FormGroup>
        <Label>Title</Label>
        <Input id='title' />
      </FormGroup>
      <FormGroup>
        <Label>description</Label>
        <Input 
          type='textarea' 
          rows='5'
          id='description' />
      </FormGroup>

      <Button color='primary'>Add</Button>
    </Form>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    addTask: (data) => {
      dispatch(addTaskAction(data))
    }
  }
}

export default connect(null, mapDispatchToProps)(TaskForm)