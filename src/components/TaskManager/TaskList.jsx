import React from 'react';
import { connect } from 'react-redux'

import { ListGroup } from 'reactstrap'
import TaskListItem from './TaskListItem'


const TaskList = ({ tasks }) => {
  
  return (
    <ListGroup>
      {tasks.map(item => (
        <TaskListItem task={{ ...item }} />
      ))}
    </ListGroup>
  )
}

const mapStateToProps = (state) => {
  return {
    tasks: state.tasks
  }
}

export default connect(mapStateToProps)(TaskList);