import React, { useState, useEffect } from 'react';

import { Row, Col } from 'reactstrap';
import TaskForm from './TaskForm'
import TaskList from './TaskList'

const TaskManager = () => {  

  return (
    <>
      <Row>
        <Col>
          <TaskForm />
        </Col>
        <Col>
          <TaskList />
        </Col>
      </Row>
    </>
  )
}

export default TaskManager;