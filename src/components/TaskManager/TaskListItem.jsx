import React from 'react';
import { connect } from 'react-redux'
import { deleteTaskAction } from '../../actions/taskActions'
import {
  ListGroupItem, Button
} from 'reactstrap'

const TaskListItem = (props) => {
  const { task } = props
  const handlerDeleteTask = () => {
    props.deleteTask(task.id)
  }

  return (
    <ListGroupItem>
      {task.title}
      <Button close onClick={handlerDeleteTask} />
    </ListGroupItem>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteTask: (id) => {
      dispatch(deleteTaskAction(id))
    }
  }
}

export default connect(null, mapDispatchToProps)(TaskListItem)